package chatgg.chatgg;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
public class GGGUI extends UI {

    @Autowired
    private DialogService dialogService;
    @Autowired
    private HistroyService histroyService;

    private TextArea textArea = new TextArea("");



    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VerticalLayout components =  new VerticalLayout();
        TextArea textArea = new TextArea("");
        TextField textField = new TextField("");

        Button button = new Button("Wyślij");
        Button buttonResresh = new Button("Odśwież");

        button.addClickListener(clickEvent -> {
            dialogService.sentMessage(textField.getValue());
            histroyService.addHistory(textField.getValue() + System.lineSeparator());
            textArea.setValue(histroyService.getHistory());
        });

        buttonResresh.addClickListener(clickEvent -> {
            textArea.setValue(histroyService.getHistory());
        });

        components.addComponent(textArea);
        components.addComponent(textField);
        components.addComponent(button);
        components.addComponent(buttonResresh);
        setContent(components);
    }


}