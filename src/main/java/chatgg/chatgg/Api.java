package chatgg.chatgg;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

    @Autowired
    private HistroyService histroyService;

    @RequestMapping("/getMessage")
    @ResponseBody
    public void sentText(@RequestParam("text") String text) {
        histroyService.addHistory(text);



    }



}
