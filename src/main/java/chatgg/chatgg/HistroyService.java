package chatgg.chatgg;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class HistroyService {

    private String history = "";

    public String getHistory() {
        return history;
    }


    public void addHistory(@RequestParam("text") String text) {
        this.history += text + System.lineSeparator();
    }
}