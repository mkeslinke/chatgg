package chatgg.chatgg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Service
public class DialogService {


    HistroyService histroyService;

    @Autowired
    public DialogService(HistroyService histroyService) {
        this.histroyService = histroyService;
    }

    public void sentMessage(String infoToSent) {
        RestTemplate rest = new RestTemplate();
        HttpEntity<String> rssResponse = rest.exchange(
                "http://localhost:8080/getMessage?text=" + infoToSent,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);

    }
}