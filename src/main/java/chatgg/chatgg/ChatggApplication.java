package chatgg.chatgg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatggApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatggApplication.class, args);
	}
}
